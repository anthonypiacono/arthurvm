#include "MachineState.h"

MachineState::MachineState() {
	ByteCode = NULL;
	ErrorState = ERROR_STATE_OK;
	ProgramLength = 0;

	for(int i = 0; i < REGISTER_COUNT; i++) {
		 Registers[i] = 0;
	}

	for(int i = 0; i < FLAG_COUNT; i++) {
		Flags[i] = false;
	}
}

void MachineState::SetRegister(unsigned int id, unsigned int value) {
	if (id < 0 || id >= REGISTER_COUNT) {
		throw std::exception((std::string("Register ID #") + boost::lexical_cast<std::string>(id) + std::string(" does not exist.")).c_str());
	}

	this->Registers[id] = value;
}

unsigned int MachineState::GetRegister(unsigned int id) {
	if (id < 0 || id >= REGISTER_COUNT) {
		throw std::exception((std::string("Register ID #") + boost::lexical_cast<std::string>(id) + std::string(" does not exist.")).c_str());
	}

	return this->Registers[id];
}

unsigned char MachineState::GetInstructionParameterByte(unsigned int offset) {
	return *(this->GetCurrentInstruction() + 1 + offset);
}

unsigned int MachineState::GetInstructionParameterLong(unsigned int offset) {
	return *((unsigned int*)(this->GetCurrentInstruction() + 1 + offset));
}

unsigned int MachineState::GetInstructionParameterAddressReference(unsigned char **output, unsigned int offset) {
	auto index = this->GetCurrentInstruction() + offset;
	auto instruction = *(index + 0x1);

	if(instruction == ADDRESS_REFERENCE_ADDRESS) { // address
		*output = (unsigned char*)(*((unsigned int*)(index + 0x2)));
		return 0x5 + offset;
	}
	else if(instruction == ADDRESS_REFERENCE_ADDRESS_AT_ADDRESS) { // address at address
		*output = (unsigned char*)(*((unsigned int*)(*((unsigned int*)(index + 0x2)))));
		return 0x5 + offset;
	}
	else if(instruction == ADDRESS_REFERENCE_ADDRESS_IN_REGISTER) { // address in register
		*output = (unsigned char*)this->GetRegister(*(index + 0x2));
		return 0x2 + offset;
	}
	else if(instruction == ADDRESS_REFERENCE_ADDRESS_AT_ADDRESS_IN_REGISTER) { // address at address in register
		*output = (unsigned char*)(*((unsigned int*)this->GetRegister(*(index + 0x2))));
		return 0x2 + offset;
	}

	throw std::exception((std::string("Unrecognized address reference (") + std::string(1, (int)instruction) + std::string(" >= )") + std::string(1, ADDRESS_REFERENCE_COUNT)).c_str());
	return offset;
}

void MachineState::Error(unsigned int state) {
	this->ErrorState = state;
}

unsigned int MachineState::GetErrorState() {
	return this->ErrorState;
}

void MachineState::SetupProgram(unsigned char *byteCode, unsigned int length) {
	this->ByteCode = byteCode;
	this->ProgramLength = length;
	this->CurrentInstruction = this->ByteCode;
}

unsigned char *MachineState::GetByteCode() {
	return this->ByteCode;
}

unsigned int MachineState::GetProgramLength() {
	return this->ProgramLength;
}

void MachineState::IncrementCurrentInstruction(unsigned int bytes) {
	this->CurrentInstruction += bytes;
}

void MachineState::SetCurrentInstruction(unsigned char *address) {
	this->CurrentInstruction = address;
}

unsigned char *MachineState::GetCurrentInstruction() {
	return this->CurrentInstruction;
}

bool MachineState::GetFlag(MachineFlag machineFlag) {
	return this->Flags[machineFlag];
}

void MachineState::SetFlag(MachineFlag machineFlag, bool state) {
	this->Flags[machineFlag] = state;
}

unsigned int *MachineState::GetStack() {
	return (unsigned int*)(&(this->Stack._Get_container()[0]));
}

void MachineState::PushToStack(unsigned int value) {
	this->Stack.push(value);
	this->SetRegister(REGISTER_STACK, (unsigned int)this->GetStack());
}

unsigned int MachineState::PopOffStack() {
	auto popped = this->Stack.top();
	this->Stack.pop();
	this->SetRegister(REGISTER_STACK, (unsigned int)this->GetStack());
	return popped;
}

unsigned int MachineState::TopOfStack() {
	return this->Stack.top();
}