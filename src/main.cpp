// Virtual Machine.cpp : Defines the entry point for the console application.
//

#include "VirtualMachine.h"

// 0x90 - NOP
// 0x89 - MOV
// 0xE9 - JMP
// 0xE8 - CALL
static unsigned char test_pcode[] =
{
	OPCODE_ADD_BYTE_TO_REGISTER, 0x1, REGISTER_GENERALPURPOSE_1,
	OPCODE_NO_OPERATION,
	OPCODE_JUMP_TO_ADDRESS, ADDRESS_REFERENCE_ADDRESS, 0x0, 0x0, 0x0, 0x0,
};

void Produce_Little_Endian_For_Word(unsigned int word, unsigned char *output) {
	output[0] = (word << 24) >> 24;
	output[1] = (word << 16) >> 24;
	output[2] = (word << 8) >> 24;
	output[3] = word >> 24;
}

bool Test_OpCode_Add_Byte_At_Address_To_Register() {
	VirtualMachine vm;

	unsigned char byteArr[4];

	unsigned char testChar = 0xF0;

	Produce_Little_Endian_For_Word((unsigned int)(&testChar), byteArr);

	unsigned char byteCode[] = {
		OPCODE_ADD_BYTE_AT_ADDRESS_TO_REGISTER, ADDRESS_REFERENCE_ADDRESS, byteArr[0], byteArr[1], byteArr[2], byteArr[3], REGISTER_GENERALPURPOSE_1
	};

	vm.Execute(byteCode, sizeof(byteCode));

	return vm.GetState()->GetRegister(REGISTER_GENERALPURPOSE_1) == (unsigned int)testChar;
}

bool Test_OpCode_Add_Byte_To_Address() {
	VirtualMachine vm;

	unsigned char byteArr[4];

	unsigned char testChar = 0x0;

	Produce_Little_Endian_For_Word((unsigned int)(&testChar), byteArr);

	unsigned char byteCode[] = {
		OPCODE_ADD_BYTE_TO_ADDRESS, 0xF0, ADDRESS_REFERENCE_ADDRESS, byteArr[0], byteArr[1], byteArr[2], byteArr[3]
	};

	vm.Execute(byteCode, sizeof(byteCode));

	return testChar == 0xF0;
}

bool Test_OpCode_Add_Byte_In_Register_To_Address() {
	VirtualMachine vm;

	unsigned char byteArr[4];

	unsigned char testChar = 0x0;

	Produce_Little_Endian_For_Word((unsigned int)(&testChar), byteArr);

	unsigned char byteCode[] = {
		OPCODE_ADD_BYTE_TO_REGISTER, 0x01, REGISTER_GENERALPURPOSE_1,
		OPCODE_ADD_BYTE_IN_REGISTER_TO_ADDRESS, REGISTER_GENERALPURPOSE_1, ADDRESS_REFERENCE_ADDRESS, byteArr[0], byteArr[1], byteArr[2], byteArr[3]
	};

	vm.Execute(byteCode, sizeof(byteCode));

	return testChar == 0x01;
}

bool Test_OpCode_Add_Byte_At_Address_To_Address() {
	VirtualMachine vm;

	unsigned char byteArr[4];
	unsigned char byteArr2[4];

	unsigned char testChar = 0x0;
	unsigned char testChar2 = 0xF0;

	Produce_Little_Endian_For_Word((unsigned int)(&testChar), byteArr);
	Produce_Little_Endian_For_Word((unsigned int)(&testChar2), byteArr2);

	unsigned char byteCode[] = {
		OPCODE_ADD_BYTE_AT_ADDRESS_TO_ADDRESS, ADDRESS_REFERENCE_ADDRESS, byteArr2[0], byteArr2[1], byteArr2[2], byteArr2[3], ADDRESS_REFERENCE_ADDRESS, byteArr[0], byteArr[1], byteArr[2], byteArr[3]
	};

	vm.Execute(byteCode, sizeof(byteCode));

	return testChar == 0xF0;
}

bool Test_OpCode_Add_Word_At_Address_To_Address() {
	VirtualMachine vm;

	unsigned char byteArr[4];
	unsigned char byteArr2[4];

	unsigned int testWord = 0x0;
	unsigned int testWord2 = 0x11111111;

	Produce_Little_Endian_For_Word((unsigned int)(&testWord), byteArr);
	Produce_Little_Endian_For_Word((unsigned int)(&testWord2), byteArr2);

	unsigned char byteCode[] = {
		OPCODE_ADD_WORD_AT_ADDRESS_TO_ADDRESS, ADDRESS_REFERENCE_ADDRESS, byteArr2[0], byteArr2[1], byteArr2[2], byteArr2[3], ADDRESS_REFERENCE_ADDRESS, byteArr[0], byteArr[1], byteArr[2], byteArr[3],
		OPCODE_ADD_WORD_AT_ADDRESS_TO_ADDRESS, ADDRESS_REFERENCE_ADDRESS, byteArr2[0], byteArr2[1], byteArr2[2], byteArr2[3], ADDRESS_REFERENCE_ADDRESS, byteArr[0], byteArr[1], byteArr[2], byteArr[3]
	};

	vm.Execute(byteCode, sizeof(byteCode));

	return testWord == 0x22222222;
}

int main(int argc, char *argv[])
{
	VirtualMachine vm;

	vm.Execute(test_pcode, sizeof(test_pcode));

	system("PAUSE");

	return 0;
}

