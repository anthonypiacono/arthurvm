#ifndef _MACHINE_STATE_H_
#define _MACHINE_STATE_H_
#include <string>
#include <exception>
#include <stack>
#include <iostream>
#include <boost/lexical_cast.hpp>

enum MachineErrorState {
	ERROR_STATE_OK
};

enum MachineFlag {
	FLAG_CARRY,
	FLAG_ZERO,
	FLAG_OVERFLOW,
	FLAG_SIGN,
	FLAG_COUNT
};

enum MachineRegister {
	REGISTER_STACK = 0,
	REGISTER_RETURNVALUE,
	REGISTER_GENERALPURPOSE_1,
	REGISTER_GENERALPURPOSE_2,
	REGISTER_GENERALPURPOSE_3,
	REGISTER_GENERALPURPOSE_4,
	REGISTER_COUNT
};

enum MachineAddressReference {
	ADDRESS_REFERENCE_ADDRESS,
	ADDRESS_REFERENCE_ADDRESS_AT_ADDRESS,
	ADDRESS_REFERENCE_ADDRESS_IN_REGISTER,
	ADDRESS_REFERENCE_ADDRESS_AT_ADDRESS_IN_REGISTER,
	ADDRESS_REFERENCE_COUNT
};

class MachineState
{
public:
	MachineState();
	void SetRegister(unsigned int id, unsigned int value);
	unsigned int GetRegister(unsigned int id);
	void Error(unsigned int state);
	unsigned int GetErrorState();
	void SetupProgram(unsigned char *byteCode, unsigned int length);
	unsigned char *GetByteCode();
	unsigned int GetProgramLength();
	void IncrementCurrentInstruction(unsigned int bytes = 1);
	void SetCurrentInstruction(unsigned char * address);
	unsigned char *GetCurrentInstruction();
	unsigned char GetInstructionParameterByte(unsigned int offset = 0);
	unsigned int GetInstructionParameterLong(unsigned int offset = 0);
	unsigned int GetInstructionParameterAddressReference(unsigned char **output, unsigned int offset = 0);
	bool GetFlag(MachineFlag machineFlag);
	void SetFlag(MachineFlag machineFlag, bool state = true);

	unsigned int *GetStack();
	void PushToStack(unsigned int value);
	unsigned int PopOffStack();
	unsigned int TopOfStack();

private:
	unsigned int Registers[REGISTER_COUNT];
	unsigned int ErrorState;
	unsigned char *ByteCode;
	unsigned int ProgramLength;
	unsigned char *CurrentInstruction;
	bool Flags[FLAG_COUNT];

	std::stack<unsigned int> Stack;
};
#endif