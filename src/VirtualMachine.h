#ifndef __VIRTUALMACHINE_H__
#define __VIRTUALMACHINE_H__

#include <iostream>

#include "MachineState.h"
#include <map>

typedef bool (*OpCodeCallback)(MachineState *);

enum OpCode {
	OPCODE_NO_OPERATION, // []

	OPCODE_ADD_BYTE_TO_REGISTER, // [Byte] [Register ID]
	OPCODE_ADD_BYTE_IN_REGISTER_TO_REGISTER, // [Register ID] [Register ID]
	OPCODE_ADD_BYTE_AT_ADDRESS_TO_REGISTER, // {Address Reference} [Register ID]
	
	OPCODE_ADD_BYTE_TO_ADDRESS, // [Byte] {Address Reference}
	OPCODE_ADD_BYTE_IN_REGISTER_TO_ADDRESS, // [Register ID] {Address Reference}
	OPCODE_ADD_BYTE_AT_ADDRESS_TO_ADDRESS, // {Address Reference} {Address Reference}

	OPCODE_ADD_WORD_TO_REGISTER, // [Word] [Register ID]
	OPCODE_ADD_WORD_IN_REGISTER_TO_REGISTER, // [Register ID] [Register ID]
	OPCODE_ADD_WORD_AT_ADDRESS_TO_REGISTER, // {Address Reference} [Register ID]

	OPCODE_ADD_WORD_TO_ADDRESS, // [Word] {Address Reference}
	OPCODE_ADD_WORD_IN_REGISTER_TO_ADDRESS, // [Register ID] {Address Reference}
	OPCODE_ADD_WORD_AT_ADDRESS_TO_ADDRESS, // {Address Reference} {Address Reference}

	OPCODE_ADD_FLOAT_TO_REGISTER, // [Float] [Register ID]
	OPCODE_ADD_FLOAT_IN_REGISTER_TO_REGISTER, // [Register ID] [Register ID]
	OPCODE_ADD_FLOAT_AT_ADDRESS_TO_REGISTER, // {Address Reference} [Register ID]

	OPCODE_ADD_FLOAT_TO_ADDRESS, // [Float] {Address Reference}
	OPCODE_ADD_FLOAT_IN_REGISTER_TO_ADDRESS, // [Register ID] {Address Reference}
	OPCODE_ADD_FLOAT_AT_ADDRESS_TO_ADDRESS, // {Address Reference} {Address Reference}

	OPCODE_ADD_DOUBLE_TO_ADDRESS, // [Double] {Address Reference}
	OPCODE_ADD_DOUBLE_IN_REGISTER_TO_ADDRESS, // [Register ID] {Address Reference}
	OPCODE_ADD_DOUBLE_AT_ADDRESS_TO_ADDRESS, // {Address Reference} {Address Reference}

	// All addresses for OPCODE_JUMP_TO_ADDRESS* and OPCODE_CALL_ADDRESS* operations are relative to machineState->GetByteCode()
	OPCODE_JUMP_TO_ADDRESS, // {Address Reference}

	OPCODE_JUMP_TO_ADDRESS_IF_FLAG, // {Address Reference} [Flag ID]
	OPCODE_JUMP_TO_ADDRESS_IF_FLAG_NOT, // {Address Reference} [Flag ID]

	OPCODE_SET_REGISTER_TO_WORD, // [Register ID] [Word]
	OPCODE_SET_REGISTER_TO_WORD_AT_ADDRESS, // [Register ID] {Address reference}
	OPCODE_SET_ADDRESS_TO_WORD, // {Address reference} [Word]
	OPCODE_SET_ADDRESS_TO_WORD_AT_ADDRESS, // {Address reference} {Address reference}

	OPCODE_SET_REGISTER_TO_BYTE, // [Register ID] [Word]
	OPCODE_SET_REGISTER_TO_BYTE_AT_ADDRESS, // [Register ID] {Address reference}
	OPCODE_SET_ADDRESS_TO_BYTE, // {Address reference} [Word]
	OPCODE_SET_ADDRESS_TO_BYTE_AT_ADDRESS, // {Address reference} {Address reference}

	OPCODE_COUNT
};

class VirtualMachine {
public:
	VirtualMachine();
	~VirtualMachine();

	void Execute(unsigned char *byteCode, unsigned int programLength);

	MachineState *GetState() { return &(this->State); }

private:
	MachineState State;
};

#endif