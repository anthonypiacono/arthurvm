#include "VirtualMachine.h"

bool OpCode_No_Operation(MachineState *machineState) {
	std::cout << "NOP!";

	machineState->IncrementCurrentInstruction();
	return true;
}

bool OpCode_Jump_To_Address(MachineState *machineState) {
	unsigned char *address;
	machineState->GetInstructionParameterAddressReference(&address);

	machineState->SetCurrentInstruction(machineState->GetByteCode() + (unsigned int)address);
	return true;
}

bool OpCode_Jump_To_Address_If_Flag(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address);
	if(machineState->GetFlag((MachineFlag)machineState->GetInstructionParameterByte(offset))) {
		machineState->SetCurrentInstruction(machineState->GetByteCode() + (unsigned int)address);
		return true;
	}

	machineState->IncrementCurrentInstruction(offset + 2);
	return true;
}

bool OpCode_Jump_To_Address_If_Flag_Not(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address);
	if(!machineState->GetFlag((MachineFlag)machineState->GetInstructionParameterByte(offset))) {
		machineState->SetCurrentInstruction(machineState->GetByteCode() + (unsigned int)address);
		return true;
	}

	machineState->IncrementCurrentInstruction(offset + 2);
	return true;
}

bool OpCode_Add_Byte_To_Register(MachineState *machineState) {
	auto registerID = machineState->GetInstructionParameterByte(1);
	machineState->SetRegister(registerID, machineState->GetRegister(registerID) + (unsigned int)machineState->GetInstructionParameterByte());

	machineState->IncrementCurrentInstruction(3);
	return true;
}

bool OpCode_Add_Byte_In_Register_To_Register(MachineState *machineState) {
	auto registerID = machineState->GetInstructionParameterByte(1);
	machineState->SetRegister(registerID, machineState->GetRegister(registerID) + machineState->GetRegister(machineState->GetInstructionParameterByte()));

	machineState->IncrementCurrentInstruction(3);
	return true;
}

bool OpCode_Add_Byte_At_Address_To_Register(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address);
	auto registerID = machineState->GetInstructionParameterByte(offset);
	machineState->SetRegister(registerID, machineState->GetRegister(registerID) + (unsigned int)(*address));

	machineState->IncrementCurrentInstruction(offset + 2);
	return true;
}

bool OpCode_Add_Byte_To_Address(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address, 1);

	*address += machineState->GetInstructionParameterByte();
	machineState->IncrementCurrentInstruction(offset + 1);
	return true;
}

bool OpCode_Add_Byte_In_Register_To_Address(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address, 1);

	*address += (unsigned char)machineState->GetRegister(machineState->GetInstructionParameterByte());
	machineState->IncrementCurrentInstruction(offset + 1);
	return true;
}

bool OpCode_Add_Byte_At_Address_To_Address(MachineState *machineState) {
	unsigned char *firstAddress, *secondAddress;
	auto offset = machineState->GetInstructionParameterAddressReference(&firstAddress);
	offset = machineState->GetInstructionParameterAddressReference(&secondAddress, offset);

	*secondAddress += *firstAddress;
	machineState->IncrementCurrentInstruction(offset + 1);
	return true;
}

////
bool OpCode_Add_Word_To_Register(MachineState *machineState) {
	auto registerID = machineState->GetInstructionParameterByte(4);
	machineState->SetRegister(registerID, machineState->GetRegister(registerID) + machineState->GetInstructionParameterLong());

	machineState->IncrementCurrentInstruction(6);
	return true;
}

bool OpCode_Add_Word_In_Register_To_Register(MachineState *machineState) {
	auto registerID = machineState->GetInstructionParameterByte(1);
	machineState->SetRegister(registerID, machineState->GetRegister(registerID) + machineState->GetRegister(machineState->GetInstructionParameterByte()));

	machineState->IncrementCurrentInstruction(3);
	return true;
}

bool OpCode_Add_Word_At_Address_To_Register(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address);
	auto registerID = machineState->GetInstructionParameterByte(offset);
	machineState->SetRegister(registerID, machineState->GetRegister(registerID) + *((unsigned int*)address));

	machineState->IncrementCurrentInstruction(offset + 2);
	return true;
}

bool OpCode_Add_Word_To_Address(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address, 4);

	*address += machineState->GetInstructionParameterLong();
	machineState->IncrementCurrentInstruction(offset + 1);
	return true;
}

bool OpCode_Add_Word_In_Register_To_Address(MachineState *machineState) {
	unsigned char *address;
	auto offset = machineState->GetInstructionParameterAddressReference(&address, 1);

	*((unsigned int*)address) += machineState->GetRegister(machineState->GetInstructionParameterByte());
	machineState->IncrementCurrentInstruction(offset + 1);
	return true;
}

bool OpCode_Add_Word_At_Address_To_Address(MachineState *machineState) {
	unsigned char *firstAddress, *secondAddress;
	auto offset = machineState->GetInstructionParameterAddressReference(&firstAddress);
	offset = machineState->GetInstructionParameterAddressReference(&secondAddress, offset);

	*((unsigned int*)secondAddress) += *((unsigned int*)firstAddress);
	machineState->IncrementCurrentInstruction(offset + 1);
	return true;
}
////

/*
OPCODE_NOP,
OPCODE_MOV,
OPCODE_JMP,
OPCODE_CALL,
OPCODE_JE,
OPCODE_JNE,
OPCODE_ADD,
OPCODE_ADDF,
OPCODE_PUSH,
OPCODE_TOP,
OPCODE_POP
*/
static OpCodeCallback OpCodes[OPCODE_COUNT] = {
	OpCode_No_Operation,
	OpCode_Add_Byte_To_Register, // OpCode_Add_Byte_To_Register,
	OpCode_Add_Byte_In_Register_To_Register, // OpCode_Add_Byte_In_Register_To_Register,
	OpCode_Add_Byte_At_Address_To_Register, // OpCode_Add_Byte_At_Address_To_Register,
	OpCode_Add_Byte_To_Address, // OpCode_Add_Byte_To_Address,
	OpCode_Add_Byte_In_Register_To_Address, // OpCode_Add_Byte_In_Register_To_Address,
	OpCode_Add_Byte_At_Address_To_Address, // OpCode_Add_Byte_At_Address_To_Address,
	OpCode_Add_Word_To_Register, // OpCode_Add_Word_To_Register,
	OpCode_Add_Word_In_Register_To_Register, // OpCode_Add_Word_In_Register_To_Register,
	OpCode_Add_Word_At_Address_To_Register, // OpCode_Add_Word_At_Address_To_Register,
	OpCode_Add_Word_To_Address, // OpCode_Add_Word_To_Address,
	OpCode_Add_Word_In_Register_To_Address, // OpCode_Add_Word_In_Register_To_Address,
	OpCode_Add_Word_At_Address_To_Address, // OpCode_Add_Word_At_Address_To_Address,
	OpCode_No_Operation, // OpCode_Add_Float_To_Register,
	OpCode_No_Operation, // OpCode_Add_Float_In_Register_To_Register,
	OpCode_No_Operation, // OpCode_Add_Float_At_Address_To_Register,
	OpCode_No_Operation, // OpCode_Add_Float_To_Address,
	OpCode_No_Operation, // OpCode_Add_Float_In_Register_To_Address,
	OpCode_No_Operation, // OpCode_Add_Float_At_Address_To_Address,
	OpCode_No_Operation, // OpCode_Add_Double_To_Address,
	OpCode_No_Operation, // OpCode_Add_Double_In_Register_To_Address,
	OpCode_No_Operation, // OpCode_Add_Double_At_Address_To_Address
	OpCode_Jump_To_Address,
	OpCode_Jump_To_Address_If_Flag,
	OpCode_Jump_To_Address_If_Flag_Not
};

VirtualMachine::VirtualMachine() {
}

VirtualMachine::~VirtualMachine() {
}

void VirtualMachine::Execute(unsigned char *byteCode, unsigned int programLength) {
	this->State.SetupProgram(byteCode, programLength);

	while(this->State.GetErrorState() == ERROR_STATE_OK) {
		auto opCode = *this->State.GetCurrentInstruction();

		if(opCode > OPCODE_COUNT) {
			throw std::exception("OpCode out of range");
		}

		if(!OpCodes[opCode](&this->State)) {
			break;
		}

		if(this->State.GetCurrentInstruction() >= (this->State.GetByteCode() + this->State.GetProgramLength())) {
			break;
		}
	}
}